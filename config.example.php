<?php
return [
    'debug' => false,
    'types' => [
        'simple' => [
            'transform' => [
                Preview::FIX_HEIGHT => 320,
            ],
            'allowed' => '#^images/gallery/#',
        ],
        'micro' => [
            'transform' => [
                Preview::FIX_SIZE => [100, 150],
                Preview::FILTER_GREY => true,
            ],
        ],
    ],
];
