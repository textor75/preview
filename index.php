<?php
Error_Reporting(E_ALL & ~E_NOTICE);

/**
 * Class Preview
 */
class Preview
{
    const ORIGINAL = 'original';
	const FIX_SIZE = 'size';
	const MAX_SIZE = 'max';
	const FIX_WIDTH = 'width';
	const FIX_HEIGHT = 'height';

	const FILTER_GREY = 'grey';
	const FILTER_NEGATIVE = 'negative';
	const FILTER_3_COLORS = '3colors';
	const FILTER_SEPIA = 'sepia';
	const FILTER_BLUR = 'blur';
	const FILTER_MOSAIC = 'mosaic';
    const FILTER_OLD_TV = 'old_tv';
    const FILTER_TILT_SHIFT = 'tilt_shift';
    const FILTER_ART = 'art';
    const FILTER_MULT = 'mult';

	const ALIGN_LEFT = 'left';
	const ALIGN_CENTER = 'center';
	const ALIGN_RIGHT = 'right';
    const VALIGN_TOP = 'top';
    const VALIGN_MIDDLE = 'middle';
    const VALIGN_BOTTOM = 'bottom';

    /**
     * @var Preview $instance
     */
	protected static $instance;
    /**
     * @var bool $debug
     */
	protected static $debug = false;
    /**
     * @var array $types
     */
	protected $types;

    /**
     * @var string $root
     */
	protected $root;
    /**
     * @var string $root
     */
	protected $previewPath;
    /**
     * @var string $root
     */
	protected $previewUri;

    /**
     * @var string $type
     */
	protected $type;
    /**
     * @var array $format
     */
	protected $format;

    /**
     * @var string $file
     */
	protected $file;
    /**
     * @var string $filePath
     */
	protected $filePath;

    /**
     * @var string $cachePath
     */
	protected $cachePath;
    /**
     * @var string $cacheUri
     */
	protected $cacheUri;

    /**
     * @var integer $imageType
     * @see php constants IMAGETYPE_*
     */
	protected $imageType;
    /**
     * @var resource $original
     */

	protected $preview;

    /**
     * Init method
     */
	public static function init()
	{
	    try {
            clearstatcache();

            $configFile = __DIR__ . '/config.php';
            if (!is_file($configFile)) {
                throw new PreviewException('There is not configuration file.');
            }
            $config = include $configFile;
            if (!empty($config['debug'])) {
                static::$debug = true;
            }
            static::$instance = new static($config);

            static::$instance->make();
        } catch (Exception $e) {
            static::bye($e->getMessage());
        }
	}

    /**
     * Pre-exit method
     * @param string $message
     */
	protected static function bye($message)
	{
		$date = new DateTime();
		header('Cache-Control: no-cache, must-revalidate', TRUE);
		header('Expires: ' . $date->format('r'), FALSE);
		if (static::$debug) {
			echo $message;
		} else {
			header('HTTP/1.0 404 Not Found', FALSE, 404);
		}
		die;
	}

    /**
     * Make preview image
     * @throws Exception
     */
	protected function make()
	{
		$this->parseRequest();
        if (is_file($this->getCachePath())) {
            $this->out();
        }
		$this->transform();
        $this->save();
		$this->out();
	}

    /**
     * Save preview image as file
     * @return string
     * @throws Exception
     */
	protected function save()
	{
		$file = $this->getCachePath();
		$folder = dirname($file);

		if (!is_dir($folder)) {
			mkdir($folder, 0777, true);
		}
		switch ($this->getImageType()) {
			case IMAGETYPE_GIF:  imagegif($this->preview, $file); break;
			case IMAGETYPE_JPEG: imagejpeg($this->preview, $file, 92); break;
			case IMAGETYPE_PNG:  imagepng($this->preview, $file); break;
		}

		return $file;
	}

    /**
     * @throws Exception
     */
	protected function out()
    {
        $file = $this->getCachePath();
        switch ($this->getImageType()) {
            case IMAGETYPE_GIF:  header('Content-type: image/gif', true); ; break;
            case IMAGETYPE_JPEG: header('Content-type: image/jpeg', true); ; break;
            case IMAGETYPE_PNG:  header('Content-type: image/png', true); ; break;
        }
        header('Expires: 0', false);
        header('Cache-Control: no-cache', false);
        header('Content-Length: ' . filesize($file), false);
        if (static::$debug) {
            header('X-Preview: PHP', false);
        }
        echo file_get_contents($file);
        die;
    }

    /**
     * Call transformation methods
     *
     * @return resource
     * @throws Exception
     */
	protected function transform()
	{
		if (!is_array($this->format['transform'])) {
			throw new PreviewException('Incorrect transformation methods parameter.');
		}
		
		$source = $this->getOriginal();
		
		foreach ($this->format['transform'] as $transform => $params) {
			if ($params === false) {
				continue;	
			}
			if (!is_array($params)) {
				$params = [$params];	
			}
			$method = 'transform_' . $transform;
			if (!method_exists($this, $method)) {
				throw new PreviewException('Unknown transformation method %s.', $transform);
			}
			
			$source = call_user_func_array([$this, $method], array_merge([$source], $params));
		}
		
		return $this->preview = $source;
		
	}

    /**
     * @return resource
     * @throws Exception
     */
	protected function getOriginal()
	{
	    $result = null;
        switch ($this->getImageType()) {
            case IMAGETYPE_GIF:
                $result = imagecreatefromgif($this->filePath);
                imagesavealpha($result, true);
                break;
            case IMAGETYPE_JPEG:
                $result = imagecreatefromjpeg($this->filePath);
                break;
            case IMAGETYPE_PNG:
                $result = imagecreatefrompng($this->filePath);
                imagesavealpha($result, true);
                break;
        }
        if (!$result) {
            throw new PreviewException('Unknown type of the original image.');
        }

        return $result;
	}

    /**
     * @return int
     * @throws Exception
     */
	protected function getImageType()
	{
		if (empty($this->imageType)) {
			$info = getimagesize($this->filePath);
			if (empty($info[2]) || !in_array($info[2], array(IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG))) {
				throw new PreviewException('Unknown format of image.');
			}
			$this->imageType = $info[2];	
		}
		
		return $this->imageType;
	}

    /**
     * Parse HTTP-request
     * @throws Exception
     */
	protected function parseRequest()
	{
		$request = $_SERVER['REQUEST_URI'];
		
		if (strpos($request, $this->previewUri) !== 0) {
			throw new PreviewException('The request does not match with preview Url.');
		}
		$request = substr($request, strlen($this->previewUri));
		$tmp = explode('/', $request);
		$this->type = array_shift($tmp);
		$this->file = implode('/', $tmp);
		$this->file = str_replace(':', '', $this->file);
		$this->file = str_replace('../', '', $this->file);
		$this->file = preg_replace('#/{2,}#m', '/', $this->file);
		if (empty($this->types[$this->type])) {
			throw new PreviewException('Unknown type: %s.', $this->type);
		}
		$this->format = $this->types[$this->type];
		$this->filePath = $this->root . '/' . $this->file;
		if (!is_file($this->filePath)) {
			throw new PreviewException('There is not file %s.', $this->file);
		}
		
		if (isset($this->format['allowed']) && !preg_match($this->format['allowed'], $this->file)) {
			throw new PreviewException('The type %s is disallowed for file %s', $this->type, $this->file);
		}
	}

    /**
     * @return string
     */
	protected function getCachePath()
	{
		if (empty($this->cachePath)) {
			$this->cachePath = $this->previewPath . '/' . $this->type . '/' . $this->file;
		}
		return $this->cachePath;
	}

    /**
     * @return string
     */
	protected function getCacheUri()
	{
		if (empty($this->cacheUri)) {
			$this->cacheUri = $this->previewUri . $this->type . '/' . $this->file;
		}
		return $this->cacheUri;
	}

    /**
     * Redirect to file if it exists
     */
	protected function redirectIfExists()
	{
		if (is_file($this->getCachePath())) {
			$this->redirect();
		}

		return false;
	}

    /**
     * Make redirect to file path
     */
	protected function redirect()
	{
		header('Location: '. $this->getCacheUri(), FALSE, 302);
		die;
	}

    /**
     * Preview constructor.
     * @param array $config
     * @throws Exception
     */
	protected function __construct(array $config)
	{
		$this->root = empty($config['root']) ? $_SERVER['DOCUMENT_ROOT'] : $config['root'];
		$this->previewPath = __DIR__;
		if (empty($config['types'])) {
			throw new PreviewException('There is not types parameter in the configuration.');
		}
		$this->types = $config['types'];
		$this->previewUri = isset($config['url']) ? $config['url'] : '/preview/';
		if (substr($this->previewUri, -1) !== '/') {
			$this->previewUri .= '/';
		}
	}

	// ### FILTER TOOLS ###

    /**
     * @param array $rgb
     * @return float
     */
	protected function getLightness($rgb)
	{
		return ($rgb[0] * 0.894 + $rgb[1] * 1.765 + $rgb[2] * 0.329)/3;
	}

    /**
     * @param integer $color
     * @return array
     */
	protected function getRGB($color)
	{
		$b = $color % 256;
		$g = ($color / 256) % 256;
		$r = ($color / 65536) % 256;
		return [$r, $g, $b];
	}

    /**
     * @param array $rgb
     * @return int
     */
	protected function getColor($rgb)
	{
		$rgb = $this->normalizeRGB($rgb);
		return $rgb[0] * 65536 + $rgb[1] * 256 + $rgb[2];
	}

    /**
     * @param array $rgb
     * @return array
     */
	protected function normalizeRGB($rgb)
	{
		for ($c = 0; $c < 3; $c++) {
			$color = round($rgb[$c]);
			if ($color < 0) {
				$color = 0;
			}
			elseif ($color > 255) {
				$color = 255;
			}
			$rgb[$c] = $color;
		}
		
		return $rgb;
	}

    /**
     * @param resource $source
     * @param int $x
     * @param int $y
     * @return array
     */
	protected function getPixel($source, $x, $y)
	{
		$color = imagecolorat($source, $x, $y);
		
		return $this->getRGB($color);
	}

    /**
     * @param resource $source
     * @param int $x
     * @param int $y
     * @param array $rgb
     * @return $this
     */
	protected function setPixel($source, $x, $y, $rgb)
	{
		$color = $this->getColor($rgb);
		imagesetpixel($source, $x, $y, $color);
		
		return $this;
	}

    /**
     * @param int $width
     * @param int $height
     * @return resource
     */
    protected function createCanvas($width, $height)
    {
        $result = imagecreatetruecolor($width, $height);
        imagefill($result, 0, 0, 0xFFFFFF);

        return $result;
    }

    protected function copyImage($source)
    {
        $width = imagesx($source);
        $height = imagesy($source);

        $result = $this->createCanvas($width, $height);
        imagecopy($result, $source, 0, 0, 0, 0, $width, $height);

        return $result;
    }

    /**
     * @param int $color1
     * @param int $color2
     * @param int $value
     * @param int $max
     * @param int $min
     * @return float
     * @throws Exception
     */
    protected function getProportion($color1, $color2, $value, $max, $min = 0)
    {
        if ($max === $min) {
            throw new PreviewException('Wrong parameters max and min for proportion.');
        }
        if (abs($value - $min) < 0.000001 || $color1 === $color2) {
            return $color1;
        }
        if ($value === $max) {
            return $color2;
        }

        $relation = ($max - $min) / ($value - $min);

        return ($color2 - $color1) / $relation + $color1;
    }

    /**
     * @param array $values
     * @return float
     */
    protected function getAverage(array $values) {
        return empty($values) ? 0 : array_sum($values) / count($values);
    }

    /**
     * @param $source
     * @param int $size
     * @return resource
     * @throws Exception
     */
    protected function getBlur($source, $size = 3)
    {
        settype($size, 'int');
        if ($size <= 1) {
            throw new PreviewException('Parameter for blurring must be 2 or more');
        }

        $shift = floor(($size - 1) / 2);

        $width = imagesx($source);
        $height = imagesy($source);
        $result = $this->createCanvas($width, $height);
        for ($y = 0; $y < $height; $y++) {
            for ($x = 0; $x < $width; $x++) {
                $r = $g = $b = [];
                for ($ry = 0; $ry < $size; $ry++) {
                    $dy = $y + $ry - $shift;
                    if ($dy < 0 || $dy >= $height) {
                        continue;
                    }
                    for ($rx = 0; $rx < $size; $rx++) {
                        $dx = $x + $rx - $shift;
                        if ($dx < 0 || $dx >= $width) {
                            continue;
                        }
                        list($r[], $g[], $b[]) = $this->getPixel($source, $dx, $dy);
                    }
                }
                $this->setPixel($result, $x, $y, [
                    $this->getAverage($r),
                    $this->getAverage($g),
                    $this->getAverage($b),
                ]);
            }
        }

        return $result;
    }

    /**
     * @param resource $source
     * @param int $tileWidth
     * @param int $tileHeight
     * @return resource
     * @throws Exception
     */
    protected function getCompressed($source, $tileWidth, $tileHeight)
    {
        settype($tileWidth, 'int');
        settype($tileHeight, 'int');

        if ($tileWidth < 1 || $tileHeight < 1) {
            throw new PreviewException('Width and height of tile must be 1 or more.');
        }

        $origWidth = imagesx($source);
        $origHeight = imagesy($source);

        $width = ceil($origWidth / $tileWidth);
        $height = ceil($origHeight / $tileHeight);

        $result = $this->createCanvas($width, $height);

        imagecopyresampled($result, $source, 0, 0, 0, 0, $width, $height, $origWidth, $origHeight);

        return $result;
    }

    /**
     * @param resource $compressed
     * @param int $width
     * @param int $height
     * @param int $tileWidth
     * @param int $tileHeight
     * @return resource
     */
    protected function restoreFromCompression($compressed, $width, $height, $tileWidth, $tileHeight)
    {
        $result = $this->createCanvas($width, $height);

        for ($x = 0; $x < $width; $x++) {
            for ($y = 0; $y < $height; $y++) {
                $rgb = $this->getPixel($compressed, floor($x / $tileWidth), floor($y / $tileHeight));
                $this->setPixel($result, $x, $y, $rgb);
            }
        }

        return $result;
    }

	// ### TRANSFORM METHODS ###

    /**
     * @param resource $source
     * @return resource
     */
    protected function transform_original($source)
    {
        return $source;
    }

    /**
     * @param resource $source
     * @param int $width
     * @param int $height
     * @param null|string $align
     * @param null|string $valign
     * @return resource
     * @throws Exception
     */
	protected function transform_size($source, $width = 0, $height = 0, $align = null, $valign = null)
	{
		settype($width, 'int');
		settype($height, 'int');
		if ($width <= 0) {
			throw new PreviewException('Size must be more than zero.');
		}
		if ($height <= 0) {
			$height = $width;
		}

		$origWidth = imagesx($source);
		$origHeight = imagesy($source);
		
        $result = imagecreatetruecolor($width, $height);
        $origRatio = $origWidth / $origHeight;
        $ratio = $width / $height;
        if ($origRatio > $ratio) {
			$widthPart = round($origHeight * $ratio);
			switch($align) {
                case static::ALIGN_LEFT: $x = 0; break;
                case static::ALIGN_RIGHT: $x = $origWidth - $widthPart; break;
                default: $x = round(($origWidth - $widthPart) / 2);
            }
            imagecopyresampled($result, $source, 0, 0, $x, 0, $width, $height, $widthPart, $origHeight);
		} else {
			$heightPart = round($origWidth / $ratio);
            switch($valign) {
                case static::VALIGN_TOP: $y = 0; break;
                case static::VALIGN_BOTTOM: $y = $origHeight - $heightPart; break;
                default: $y = round(($origHeight - $heightPart) / 2);
            }
            imagecopyresampled($result, $source, 0, 0, 0, $y, $width, $height, $origWidth, $heightPart);
		}
		
		return $result;
	}

    /**
     * @param resource $source
     * @param int $width
     * @param int $height
     * @return resource
     * @throws Exception
     */
	protected function transform_max($source, $width = 0, $height = 0)
	{
		settype($width, 'int');
		settype($height, 'int');
		if ($width <= 0) {
			throw new PreviewException('Maxsize must be more than zero.');
		}
		if ($height <= 0) {
			$height = $width;
		}
		
		$origWidth = imagesx($source);
		$origHeight = imagesy($source);
		
        $origRatio = $origWidth / $origHeight;
        $ratio = $width / $height;
		if ($origRatio > $ratio) {
			$height = round($width / $origRatio);
		} else {
			$width = round($height * $origRatio);
		}
		$result = imagecreatetruecolor($width, $height);
		imagecopyresampled($result, $source, 0, 0, 0, 0, $width, $height, $origWidth, $origHeight);
		
		return $result;
	}

    /**
     * @param resource $source
     * @param int $width
     * @return resource
     * @throws Exception
     */
	protected function transform_width($source, $width = 0)
	{
		settype($width, 'int');
		if ($width <= 0) {
			throw new PreviewException('Width must be more than zero.');
		}
		
		$origWidth = imagesx($source);
		$origHeight = imagesy($source);		
		
        $origRatio = $origWidth / $origHeight;
        $height = round($width / $origRatio);
        $result = imagecreatetruecolor($width, $height);
        imageCopyResampled($result, $source, 0, 0, 0, 0, $width, $height, $origWidth, $origHeight);
		
		return $result;
	}

    /**
     * @param resource $source
     * @param int $height
     * @return resource
     * @throws Exception
     */
	protected function transform_height($source, $height = 0)
	{
		settype($height, 'int');
		if ($height <= 0) {
			throw new PreviewException('Height must be more than zero.');
		}
		
		$origWidth = imagesx($source);
		$origHeight = imagesy($source);		
		
        $origRatio = $origWidth / $origHeight;
        $width = round($height * $origRatio);
        $result = imagecreatetruecolor($width, $height);
        imageCopyResampled($result, $source, 0, 0, 0, 0, $width, $height, $origWidth, $origHeight);
		
		return $result;
	}
	
	// ### FILTER METHODS ###

    /**
     * @param resource $source
     * @return resource
     */
	protected function transform_grey($source)
	{
        $result = $this->copyImage($source);
        imagefilter($result, IMG_FILTER_GRAYSCALE);
	
		return $result;
	}

    /**
     * @param resource $source
     * @return resource
     */
	protected function transform_negative($source)
	{
        $result = $this->copyImage($source);
        imagefilter($result, IMG_FILTER_NEGATE);
		
		return $result;
	}

    /**
     * @param resource $source
     * @param int $color1
     * @param int $color2
     * @param int $color3
     * @return resource
     * @throws Exception
     */
	protected function transform_3colors($source, $color1 = null, $color2 = null, $color3 = null)
    {
        if ($color1 === null || $color2 === null || $color3 === null) {
            throw new PreviewException('Filter 3 colors mast have 3 parameters.');
        }
        $width = imagesx($source);
        $height = imagesy($source);

        $matrix = [
            $this->getRGB((int)$color1),
            $this->getRGB((int)$color2),
            $this->getRGB((int)$color3),
        ];

        $result = $this->createCanvas($width, $height);
        for ($y = 0; $y < $height; $y++) {
            for ($x = 0; $x < $width; $x++) {
                $rgb = $this->getPixel($source, $x, $y);
                $lightness = $this->getLightness($rgb);
                $start = $lightness < 128 ? 0 : 1;
                $lightness -= 128 * $start;

                $this->setPixel($result, $x, $y, [
                    $this->getProportion($matrix[$start][0], $matrix[$start + 1][0], $lightness, 127),
                    $this->getProportion($matrix[$start][1], $matrix[$start + 1][1], $lightness, 127),
                    $this->getProportion($matrix[$start][2], $matrix[$start + 1][2], $lightness, 127),
                ]);
            }
        }

        return $result;
    }

    /**
     * @param resource $source
     * @return resource
     * @throws Exception
     */
    protected function transform_sepia($source)
    {
	    return $this->transform_3colors($source, 0x413c29, 0x998c5f, 0xffea9f);
    }

    /**
     * @param $source
     * @param int|bool $size
     * @return resource
     * @throws Exception
     */
    protected function transform_blur($source, $size = true)
    {
        if ($size === true) {
            $size = 3;
        }
        return $this->getBlur($source, $size);
    }

    /**
     * @param resource $source
     * @param int $tileWidth
     * @param int $tileHeight
     * @return resource
     * @throws Exception
     */
    protected function transform_mosaic($source, $tileWidth = 0, $tileHeight = 0)
    {
        settype($tileWidth, 'int');
        settype($tileHeight, 'int');

        if ($tileWidth <= 0) {
            throw new PreviewException('Size of tile must be more than zero.');
        }
        if ($tileHeight <= 0) {
            $tileHeight = $tileWidth;
        }

        $compressed = $this->getCompressed($source, $tileWidth, $tileHeight);

        $width = imagesx($source);
        $height = imagesy($source);

        return $this->restoreFromCompression($compressed, $width, $height, $tileWidth, $tileHeight);
    }

    /**
     * @param resource $source
     * @return resource
     * @throws Exception
     */
    protected function transform_old_tv($source)
    {
        $tileWidth = 5;
        $tileHeight = 2;

        $compressed = $this->transform_3colors(
            $this->getCompressed($source, $tileWidth, $tileHeight),
            0x666666,
            0xbbbbCC,
            0xEEEEFF
        );

        $width = imagesx($source);
        $height = imagesy($source);

        return $this->restoreFromCompression($compressed, $width, $height, $tileWidth, $tileHeight);
    }

    /**
     * @param resource $source
     * @return resource
     * @throws Exception
     */
    protected function transform_tilt_shift($source)
    {
        $width = imagesx($source);
        $height = imagesy($source);

        $blur = $this->getBlur($source, 7);
        $result = $this->createCanvas($width, $height);

        $heightHalf = $height / 2;

        for ($y = 0; $y < $height; $y++) {
            $vertical = $y - $heightHalf;
            if ($vertical < 0) {
                $vertical = abs($vertical);
            } else {
                $vertical++;
            }
            $k = $vertical / $heightHalf;
            for ($x = 0; $x < $width; $x++) {
                if ($k > 0.8) {
                    $rgb = $this->getPixel($blur, $x, $y);
                } elseif ($k < 0.4) {
                    $rgb = $this->getPixel($source, $x, $y);
                } else {
                    $rgbSource = $this->getPixel($source, $x, $y);
                    $rgbBlur = $this->getPixel($blur, $x, $y);
                    $rgb = [
                        $this->getProportion($rgbSource[0], $rgbBlur[0], $k, 0.8, 0.4),
                        $this->getProportion($rgbSource[1], $rgbBlur[1], $k, 0.8, 0.4),
                        $this->getProportion($rgbSource[2], $rgbBlur[2], $k, 0.8, 0.4),
                    ];
                }

                $this->setPixel($result, $x, $y, $rgb);
            }
        }

        return $result;
    }

    /**
     * @param resource $source
     * @return resource
     * @throws Exception
     */
    protected function transform_art($source)
    {
        $blurMaxPoint = 50;
        $greyMaxPoint = 60;
        $sumPointRate = 2;
        $blurParam = 5;
        $blurToBlack = [20, 50];
        $normalToBlack = 100;

        $blurSumPoint = $blurMaxPoint * $sumPointRate;
        $greySumPoint = $greyMaxPoint * $sumPointRate;
        $blurToWhite = [255 - $blurToBlack[0], 255 - $blurToBlack[1]];
        $normalToWhite = 255 - $normalToBlack;

        $width = imagesx($source);
        $height = imagesy($source);

        $blur = $this->getBlur($source, $blurParam);
        $result = $this->createCanvas($width, $height);

        for ($y = 0; $y < $height; $y++) {
            for ($x = 0; $x < $width; $x++) {
                $rgbSource = $this->getPixel($source, $x, $y);
                $rgbBlur = $this->getPixel($blur, $x, $y);

                $rgbDiff = array_map(
                    function($val1, $val2) { return abs($val1 - $val2); },
                    $rgbSource,
                    $rgbBlur
                );

                $diff = array_sum($rgbDiff);
                $max = max($rgbDiff);

                if ($diff < $blurSumPoint && $max < $blurMaxPoint) {
                    $lightness = $this->getLightness($rgbBlur);
                    if ($lightness < $blurToBlack[0]) {
                        $rgb = [0, 0, 0];
                    } elseif ($lightness < $blurToBlack[1]) {
                        $rgb = [
                            $this->getProportion(0, $rgbBlur[0], $lightness, $blurToBlack[1], $blurToBlack[0]),
                            $this->getProportion(0, $rgbBlur[1], $lightness, $blurToBlack[1], $blurToBlack[0]),
                            $this->getProportion(0, $rgbBlur[2], $lightness, $blurToBlack[1], $blurToBlack[0]),
                        ];
                    } elseif ($lightness > $blurToWhite[0]) {
                        $rgb = [255, 255, 255];
                    } elseif ($lightness > $blurToWhite[1]) {
                        $rgb = [
                            $this->getProportion($rgbBlur[0], 255, $lightness, $blurToWhite[0], $blurToWhite[1]),
                            $this->getProportion($rgbBlur[1], 255, $lightness, $blurToWhite[0], $blurToWhite[1]),
                            $this->getProportion($rgbBlur[2], 255, $lightness, $blurToWhite[0], $blurToWhite[1]),
                        ];
                    } else {
                        $rgb = $rgbBlur;
                    }
                } elseif ($diff < $greySumPoint && $max < $greyMaxPoint) {
                    $rgb = $rgbSource;
                } else {
                    $lightness = $this->getLightness($rgbSource);
                    if ($lightness < $normalToBlack) {
                        $lightness = 0;
                    } elseif ($lightness > $normalToWhite) {
                        $lightness = 255;
                    }
                    $rgb = [$lightness, $lightness, $lightness];
                }

                $this->setPixel($result, $x, $y, $rgb);
            }
        }

        return $result;
    }

    /**
     * @param resource $source
     * @return resource
     */
    protected function transform_mult($source)
    {
        $width = imagesx($source);
        $height = imagesy($source);

        $limit = 20;
        $averageLimit = 15;

        $result = $this->createCanvas($width, $height);

        $closest = [
            [-1, -1], [ 0, -1], [ 1, -1],
            [-1,  0],           [ 1,  0],
            [-1,  1], [ 0,  1], [ 1,  1],
        ];

        $done = [];

        $pixels = $height * $width;

        // see every pixel
        for ($i = 0; $i < $pixels; $i++) {
            if (isset($done[$i])) {
                continue;
            }
            $todo[$i] = true;
            $neighborsColors = [[], [], []];
            $averageColor = null;
            // see similar neighbors
            while (count($todo)) {
                $pixel = key($todo);
                $x = $pixel % $width;
                $y = floor( $pixel / $width);

                $color = $this->getPixel($source, $x, $y);
                $neighborsColors[0][] = $color[0];
                $neighborsColors[1][] = $color[1];
                $neighborsColors[2][] = $color[2];

                $neighborCount = 0;
                $similarPixelsCount = 0;

                foreach ($closest as $neighbor) {
                    $nx = $x + $neighbor[0];
                    if ($nx < 0 || $nx >= $width) {
                        continue;
                    }
                    $ny = $y + $neighbor[1];
                    if ($ny < 0 || $ny >= $height) {
                        continue;
                    }
                    $neighborCount++;
                    $nPixel = $ny * $height + $nx;
                    $nColor = $this->getPixel($source, $nx, $ny);
                    $max = max(
                        abs($nColor[0] - $color[0]),
                        abs($nColor[1] - $color[1]),
                        abs($nColor[2] - $color[2])
                    );
                    if ($max < $limit) {
                        $similarPixelsCount++;
                        if (!isset($done[$nPixel]) && !isset($todo[$nPixel])) {

                            $similar = true;
                            if ($averageColor) {
                                $max = max(
                                    abs($averageColor[0] - $nColor[0]),
                                    abs($averageColor[1] - $nColor[1]),
                                    abs($averageColor[2] - $nColor[2])
                                );
                                $similar = $max < $averageLimit;
                            }
                            if ($similar) {
                                $todo[$nPixel] = true;
                            } else {

                            }
                        }
                    }
                }
                $averageColor = [
                    $this->getAverage($neighborsColors[0]) * $similarPixelsCount / $neighborCount,
                    $this->getAverage($neighborsColors[1]) * $similarPixelsCount / $neighborCount,
                    $this->getAverage($neighborsColors[2]) * $similarPixelsCount / $neighborCount,
                ];

                $this->setPixel($result, $x, $y, $averageColor);

                unset($todo[$pixel]);
                $done[$pixel] = true;
            } // end see similar neighbors
        } // end see every pixel

        return $result;
    }
}


/**
 * Class PreviewException
 */
class PreviewException extends Exception
{
    public function __construct()
    {
        $message = call_user_func_array('sprintf', func_get_args());
        parent::__construct($message);
    }
}

Preview::init();
